#pragma once

#include "gtest/gtest.h"

#include "../../Contract/IContract.h"

using namespace quantcoin_api;

struct SmartTransactionData final : public IContractData {
  void parse(const vector_string_t& data) override {
    if (data[1] == "YES")
      is_valid = true;
    else if (data[1] == "NO")
      is_valid = false;
    else
      throw IContractException::PARSE;
  }

  [[nodiscard]] std::string get_string() const override {
    return (is_valid ? "YES" : "NO");
  }

  bool is_valid = false;
};

class SmartTransaction final : public IContract<SmartTransactionData> {
 public:
  constexpr char get_identifier() const override { return 'A'; }

  bool can_add_to_blockchain() override { return data.is_valid; }

  void set_data(const SmartTransactionData& new_data) override {
    data = new_data;
  }

  void set_data(const std::string& new_data) override {
    all_check(new_data);

    SmartTransactionData transaction_data;
    transaction_data.parse(split(new_data));

    data = transaction_data;
  }

  [[nodiscard]] SmartTransactionData get_data() const override { return data; }

  void work() override { std::cout << "Done work!" << std::endl; }

  SmartTransactionData data;
};

TEST(Contract, get_identifier) {
  SmartTransaction smart_transaction;
  EXPECT_TRUE(smart_transaction.get_identifier() == 'A');
}

TEST(Contract, set_data__string) {
  SmartTransaction smart_transaction;
  smart_transaction.set_data("A:YES");
  EXPECT_TRUE(smart_transaction.can_add_to_blockchain());
}

TEST(Contract, set_data__class) {
  SmartTransaction smart_transaction;
  SmartTransactionData data;
  data.is_valid = true;
  smart_transaction.set_data(data);

  EXPECT_TRUE(smart_transaction.can_add_to_blockchain());
}

TEST(Contract, get_data__string) {
  SmartTransaction smart_transaction;
  smart_transaction.set_data("A:YES");

  auto data_for_check = smart_transaction.get_data();
  EXPECT_TRUE(data_for_check.is_valid);
}

TEST(Contract, get_data__class) {
  SmartTransaction smart_transaction;
  SmartTransactionData data;
  data.is_valid = true;
  smart_transaction.set_data(data);

  auto data_for_check = smart_transaction.get_data();
  EXPECT_TRUE(data_for_check.is_valid);
}

TEST(Contract, get_string_NO) {
  SmartTransaction smart_transaction;
  auto lol = smart_transaction.get_string();

  ASSERT_STREQ(lol.c_str(), "A:NO");
}

TEST(Contract, get_string_YES) {
  SmartTransaction smart_transaction;
  smart_transaction.set_data("A:YES");

  auto lol = smart_transaction.get_string();

  ASSERT_STREQ(lol.c_str(), "A:YES");
}

TEST(Contract, set_data_error_STRING_IS_EMPTY) {
  try {
    SmartTransaction smart_transaction;
    smart_transaction.set_data("");
    EXPECT_TRUE(false);
  } catch (const IContractException& ex) {
    EXPECT_TRUE(ex == IContractException::STRING_IS_EMPTY);
  }
}

TEST(Contract, set_data_error_TRANSACTION_NOT_MATCH_THE_CONTRACT) {
  try {
    SmartTransaction smart_transaction;
    smart_transaction.set_data("B:YES");
    EXPECT_TRUE(false);
  } catch (const IContractException& ex) {
    EXPECT_TRUE(ex == IContractException::TRANSACTION_NOT_MATCH_THE_CONTRACT);
  }
}

TEST(Contract, set_data_error_WRONG_DELIMITER) {
  try {
    SmartTransaction smart_transaction;
    smart_transaction.set_data("A|YES");
    EXPECT_TRUE(false);
  } catch (const IContractException& ex) {
    EXPECT_TRUE(ex == IContractException::WRONG_DELIMITER);
  }
}
