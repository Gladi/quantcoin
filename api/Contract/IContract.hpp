#pragma once

#include <sstream>
#include "fmt/core.h"

#include "../Network/TrackerAPI.h"
#include "IContractData.h"
#include "IContractException.h"

namespace quantcoin_api {
//! For storage contract data
template <class for_storage_contract_data>
concept StorageContractData = requires(for_storage_contract_data data) {
  { std::is_base_of_v<IContractData, for_storage_contract_data> }
  ->std::convertible_to<bool>;
};

//! \brief Your contract
//! \details With this virtual class you can create your contract
//! \example You can check the usage examples in "test" or "example" folder
//! \details To manage your virtual contract use ContractService
template <StorageContractData storage_contract_data>
struct IContract {
  //! \breif Get identifier
  //! \details Identifier for detect your contract
  //! \example {A}:123. A - identifier
  //! \warning ONLY CONSTEXPR
  [[nodiscard]] virtual constexpr char get_identifier() const = 0;

  //! \brief Get name contract
  [[nodiscard]] virtual constexpr const char* const get_name() const = 0;

  //! \breif Get delimiter
  //! \details The delimiter is needed for the correct parsing of your
  //! transaction \example A{:}123. : - delimiter \warning ONLY CONSTEXPR
  [[nodiscard]] virtual constexpr char get_delimiter() const { return ':'; };

  //! Passing new data to a transaction
  virtual void set_data(const storage_contract_data& new_data) = 0;

  //! Passing new data to a transaction with formatting
  virtual void set_data(const std::string& data) = 0;

  //! Convert storage_contract_data to std::string
  //! \example MySuperContract -> "A:123"
  virtual std::string get_string() {
    return fmt::format("{}{}{}", this->get_identifier(), this->get_delimiter(),
                       this->get_data().get_string());
  };

  //! Check for correct formatting
  virtual void all_check(const std::string& data) {
    if (data.size() == 0)
      throw IContractException::STRING_IS_EMPTY;

    if (data[0] != get_identifier())
      throw IContractException::TRANSACTION_NOT_MATCH_THE_CONTRACT;

    if (data[1] != get_delimiter())
      throw IContractException::WRONG_DELIMITER;
  }

  //! Get storage_contract_data
  [[nodiscard]] virtual storage_contract_data get_data() const = 0;

  //! Your work
  [[nodiscard]] virtual void work() = 0;

  //! Split for formatting
  virtual std::vector<std::string> split(const std::string& string_contract) {
    return detail::split(string_contract, this->get_delimiter());
  };
};
};  // namespace quantcoin_api
