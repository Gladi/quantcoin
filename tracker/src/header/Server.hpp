#pragma once

#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/SocketAcceptor.h"
#include "Poco/Net/SocketConnector.h"
#include "Poco/Net/SocketReactor.h"

#include "ConnectionHandler.hpp"
#include "config.hpp"

using namespace Poco::Net;

struct Server {
  Server(const int& port = compiler_config::PORT) noexcept
      : port(port){};  // TODO
  ~Server() noexcept = default;

  void start() {
    ServerSocket svs(port);
    ParallelSocketAcceptor<ConnectionHandler, SocketReactor> acceptor(svs,
                                                                      reactor);

    reactor.run();  // TODO Программа заедает!
  };

  void stop() { reactor.stop(); }

 private:
  int port;
  SocketReactor reactor;
};
