#pragma once

#include <algorithm>
#include <list>
#include <mutex>
#include <random>
#include <ranges>
#include <string>
#include <vector>
#include "Poco/Net/StreamSocket.h"
#include "spdlog/spdlog.h"

using Poco::Net::StreamSocket;

using port_t = std::string;
using ip_address_t = std::string;
using info_peer_t = std::tuple<ip_address_t, port_t>;

using dictionary_of_ip_t = std::list<info_peer_t>;
using all_client_t = std::list<StreamSocket>;

struct DictionaryOfIP {
  [[nodiscard]] static bool Add(const ip_address_t& ip_address,
                                const port_t& port,
                                StreamSocket& socket);

  static void Delete(const ip_address_t& ip_address,
                     const port_t& port,
                     StreamSocket& socket);

  static std::vector<info_peer_t> GetRandom(long size_for_return);

  static void SendMessageForAllPeers(const std::string& message);

 private:
  static dictionary_of_ip_t data;
  static all_client_t all_client;
  static std::mutex mutex;
};
