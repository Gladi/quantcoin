#pragma once

#include <spdlog/spdlog.h>
#include "Poco/Net/SocketNotification.h"
#include "Poco/Net/SocketReactor.h"

#include "../../api/Network/TrackerAPI.hpp"
#include "DictionaryOfIP.hpp"
#include "config.hpp"

using namespace Poco::Net;
using Poco::Observer;

class CommandManager {
  CommandManager() = delete;
  ~CommandManager() = delete;

#define ARGS_FOR_COMMAND_MANAGER \
  StreamSocket &socket, const std::string &message
 public:
  static void Run(StreamSocket& socket, const std::string& message) {
    if (message == quantcoin_api::macros::GET_VERSION) {
      SendVersion(socket, message);
      return;
    }

    auto args =
        quantcoin_api::detail::split(message, quantcoin_api::macros::SPLIT);
    if (args[0] == quantcoin_api::detail::ADD_DICTIONARY_OF_IP)
      AddDictionary(socket, message);
    else if (args[0] ==
             quantcoin_api::detail::get_dictionary::GET_DICTIONARY_OF_IP)
      GetDictionary(socket, message);
    else if (args[0] == quantcoin_api::detail::SEND_MESSAGE_FOR_ALL_PEERS)
      SendMessageForAllPeers(socket, message);
    else
      spdlog::error("ERROR MESSAGE: {}", message);
  };

  inline static void SendMessageForAllPeers(ARGS_FOR_COMMAND_MANAGER) {
    spdlog::info("Messagess for all peers: {}", message);
    DictionaryOfIP::SendMessageForAllPeers(message);
  }

  inline static void GetDictionary(ARGS_FOR_COMMAND_MANAGER) {
    auto args =
        quantcoin_api::detail::split(message, quantcoin_api::macros::SPLIT);
    auto for_convert_to_string = DictionaryOfIP::GetRandom(std::stoi(args[1]));
    spdlog::info("for_convert_to_string.size = {}; args[1] = {}",
                 for_convert_to_string.size(), args[1]);

    // TODO BUG
    if (for_convert_to_string.empty()) {
      spdlog::info("Get dictionary: ip = {}, size = NO_IP_FOR_DICTIONARY",
                   socket.peerAddress().toString());
      socket.sendBytes(quantcoin_api::macros::NO_IP_FOR_DICTIONARY,
                       strlen(quantcoin_api::macros::NO_IP_FOR_DICTIONARY));
    } else {
      std::string for_send;
      for (const auto& [ip_address, port] : for_convert_to_string)
        for_send += fmt::format("{}{}{}{}", ip_address,
                                quantcoin_api::detail::get_dictionary::
                                    SPLIT_FOR_DICTIONARY_OF_IP_PAIR,
                                port,
                                quantcoin_api::detail::get_dictionary::
                                    SPLIT_FOR_DICTIONARY_OF_IP_VECTOR);

      spdlog::info("Get dictionary: ip = {}, size = {}",
                   socket.peerAddress().toString(), args[1]);
      socket.sendBytes(for_send.data(), for_send.size());
    }
  }

  inline static void AddDictionary(ARGS_FOR_COMMAND_MANAGER) {
    auto args =
        quantcoin_api::detail::split(message, quantcoin_api::macros::SPLIT);

    spdlog::info("Add dictionary: ip = {}, port = {}", args[1], args[2]);
    bool is_ok = DictionaryOfIP::Add(args[1], args[2], socket);

    constexpr auto YES = "Y";
    constexpr auto NO = "N";
    std::string answer;

    is_ok ? answer = YES : answer = NO;

    socket.sendBytes(answer.c_str(), answer.size());
  }

  inline static void SendVersion(ARGS_FOR_COMMAND_MANAGER) {
    spdlog::info("Command send version: {}", socket.peerAddress().toString());
    socket.sendBytes(compiler_config::VERSION,
                     strlen(compiler_config::VERSION));
  }
};

class ConnectionHandler {
 public:
  ConnectionHandler(StreamSocket& socket, SocketReactor& reactor)
      : _socket(socket), _reactor(reactor) {
    spdlog::info("New connection: ip = {}", socket.peerAddress().toString());

    _reactor.addEventHandler(_socket,
                             Observer<ConnectionHandler, ReadableNotification>(
                                 *this, &ConnectionHandler::onReadable));
  }

  ~ConnectionHandler() {
    spdlog::info("Disconnect client, port is {}", port);

    if (!port.empty() && !ip.empty())
      DictionaryOfIP::Delete(ip, port, _socket);

    _reactor.removeEventHandler(
        _socket, Observer<ConnectionHandler, ReadableNotification>(
                     *this, &ConnectionHandler::onReadable));
  }

  void onReadable(ReadableNotification* pNf) {
    pNf->release();
    char buffer[2000];
    int n = _socket.receiveBytes(buffer, sizeof(buffer));
    if (n > 0) {
      std::string message(buffer, n);
      auto args =
          quantcoin_api::detail::split(message, quantcoin_api::macros::SPLIT);
      if (args[0] == quantcoin_api::detail::ADD_DICTIONARY_OF_IP) {
        ip = args[1];
        port = args[2];
      }

      CommandManager::Run(_socket, message);
    } else {
      _socket.shutdownSend();
      delete this;
    }
  }

 private:
  StreamSocket _socket;
  SocketReactor& _reactor;
  std::string ip = "";
  std::string port = "";
};
