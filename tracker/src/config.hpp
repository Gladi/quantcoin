#pragma once

#include "spdlog/spdlog.h"

namespace compiler_config {
//! Port for connect
constexpr int PORT = 1953;

//! Log level for debug
constexpr spdlog::level::level_enum LOG_LEVEL = spdlog::level::debug;

//! Name program
constexpr const char* const NAME_PROGRAM = "Quantcoin";

//! Version
constexpr const char* const VERSION = "1.0";

};