#include "header/DictionaryOfIP.hpp"

dictionary_of_ip_t DictionaryOfIP::data;
all_client_t DictionaryOfIP::all_client;
std::mutex DictionaryOfIP::mutex;

bool DictionaryOfIP::Add(const ip_address_t& ip_address,
                         const port_t& port,
                         StreamSocket& socket) {
  std::scoped_lock<std::mutex> lock(mutex);

  for (const auto& [__ip_address, __port] : data)
    if (__ip_address == ip_address, __port == port)
      return false;

  data.push_back(std::make_tuple(ip_address, port));
  all_client.push_back(socket);
  return true;
}

std::vector<info_peer_t> DictionaryOfIP::GetRandom(long size_for_return) {
  std::scoped_lock<std::mutex> lock(mutex);
  std::vector<info_peer_t> result;

  if (size_for_return < data.size())
    size_for_return = data.size();

  std::ranges::sample(data, std::back_inserter(result), size_for_return,
                      std::mt19937{std::random_device{}()});
  return result;
}

void DictionaryOfIP::SendMessageForAllPeers(const std::string& message) {
  std::scoped_lock<std::mutex> lock(mutex);

  for (auto&& _data : data) {
    const auto [ip_address, port] = _data;
    try {
      StreamSocket socket;
      socket.connect(Poco::Net::SocketAddress(ip_address, port));
      socket.setBlocking(false);
      socket.sendBytes(message.c_str(), message.size());
      socket.shutdown();

      spdlog::info("SendMessageForAllPeers: message = {}; ip = {}; port = {}",
                   message, ip_address, port);
    } catch (Poco::Exception& ex) {
      spdlog::error(
          "Error SendMessageForAllPeers: message = {}; ip = {}; port = {}; "
          "message error = {}",
          message, ip_address, port, ex.what());
    }
  }
}

void DictionaryOfIP::Delete(const ip_address_t& ip_address,
                            const port_t& port,
                            StreamSocket& socket) {
  std::scoped_lock<std::mutex> lock(mutex);

  std::erase_if(data, [&](info_peer_t info) {
    auto [__ip_address, __port] = info;
    spdlog::info("__ip_address = {}, ip_address = {}", __ip_address,
                 ip_address);
    spdlog::info("__port = {}, port = {}", __port, port);
    return (__ip_address == ip_address && __port == port);
  });

  std::erase_if(all_client, [&](StreamSocket& __socket) {
    if (__socket == socket) {
      spdlog::info("__socket = socket");
      return true;
    }

    return false;
  });
}
