//
// Created by gladi on 05.12.2021.
//

#pragma once

#include <gtest/gtest.h>
#include "../../../src/header/QSTL/tools/value.h"

TEST(QSTL_value, init_value) {
    qstd::value<int> test;
    EXPECT_TRUE(true);
}

TEST(QSTL_value, no_exitsts) {
    qstd::value<int> test;
    EXPECT_TRUE(test.exists() == false);
}

TEST(QSTL_value, yes_exitsts) {
    qstd::value<int> test(2);
    EXPECT_TRUE(test.exists());
}

TEST(QSTL_value, get_value) {
    qstd::value<int> test;
    test = 3;

    auto test_return = *test;

    EXPECT_TRUE(test_return == 3);
}