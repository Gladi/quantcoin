//
// Created by gladi on 11.12.2021.
//

#pragma once

#include <gtest/gtest.h>
#include "../../../src/header/QSTL/tools/mutex_object.h"

TEST(QSTL_mutex_object, create_null) {
    qstd::mutex_object<int> test;
    EXPECT_TRUE(true);
};

TEST(QSTL_mutex_object, create) {
    qstd::mutex_object<int> test(43);
    EXPECT_TRUE(true);
};

