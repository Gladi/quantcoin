//
// Created by gladi on 18.12.2021.
//

#pragma once

#include <stddef.h>
#include "header/QSTL/types/types.hpp"

namespace compiler_config {
static constexpr const char* const ADDRESS_TO_TRACKER = "127.0.0.1";
static constexpr int PORT = 1953;
static constexpr qstd::constexpr_string NAME_CRYPTOCURRENCY = "Quantcoin";
static constexpr size_t SIZE_BUFFER_FOR_SOCKET = 1240; // TODO добавить в udpserver
static constexpr const char* const VERSION = "1.0";
static constexpr size_t SIZE_IPS_FOR_DICTIONARY = 100;

static constexpr const char* const PATH_TO_BLOCKCHAIN =
    "blockchain_quaintcoin01";

static constexpr unsigned int SIZE_RSA_KEY = 3072;
static constexpr const char* const PATH_TO_SETTINGS = "quantcoin01.config";

namespace DEV {

static constexpr const char* const LEVELDB_FILE_EXTENSION = ".ldb";

};

};  // namespace compiler_config
