// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/form/Main/MainWindow.hpp"
#include "../../../form/ui_MainWindow.h"

MainWindow::MainWindow(QWidget* parent)
    : QDialog(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  MainWindow::SetDesing();
  Blockchain::Open();

  if (!Blockchain::IsCurrent()) {
    ui->stackedWidget->setCurrentWidget(ui->page_sync_blockchain);
    ShowInfoAboutInfoBlockchain();
    return;
  }

  CheckHasAccount();
}

MainWindow::~MainWindow() {
  delete ui;
}

void MainWindow::on_MainWindow_destroyed() {
  Closing closing_form;
  closing_form.exec();
}

void MainWindow::on_MainWindow_finished() {
  Closing closing_form;
  closing_form.exec();
}

void MainWindow::on_create_account_bt_clicked() {
  try {
    if (HasAccount()) {
      QMessageBox::information(this, compiler_config::NAME_CRYPTOCURRENCY,
                               "Account is already exists!");
      return;
    }

    auto settings = SettingsManager::Load();
    const auto new_account_info = Blockchain::CreateAccount();
    settings.account_info = new_account_info;
    SettingsManager::Save(settings);

    const QString title = compiler_config::NAME_CRYPTOCURRENCY;
    const auto text = QString::fromStdString(
        fmt::format("Account is created!\nWallet address: {}",
                    new_account_info.wallet_address));

    QMessageBox::information(this, title, text, QMessageBox::Ok);

    CheckHasAccount();
  } catch (const std::exception& ex) {
    const auto text = QString::fromStdString(
        fmt::format("Error creating account\nError: {}", ex.what()));

    QMessageBox::critical(this, compiler_config::NAME_CRYPTOCURRENCY, text);
  }
}

void MainWindow::CheckHasAccount() {
  QWidget* page_for_open;

  if (HasAccount())
    page_for_open = ui->page_account_is_found;
  else
    page_for_open = ui->page_not_found_account;

  ui->stackedWidget->setCurrentWidget(page_for_open);
}

void MainWindow::on_copy_your_wallet_address_bt_clicked() {
  const auto wallet_address = QString::fromStdString(
      SettingsManager::Load().account_info.wallet_address);

  QClipboard* clipboard = QApplication::clipboard();

  clipboard->setText(wallet_address, QClipboard::Clipboard);

  // For Linux
  // https://www.medo64.com/2019/12/copy-to-clipboard-in-qt/#:~:text=The%20first%20time%20I%20tried,clipboard%20%3D%20QApplication%3A%3Aclipboard()%3B
  if (clipboard->supportsSelection()) {
    clipboard->setText(wallet_address, QClipboard::Selection);
  }
}

void MainWindow::ShowInfoAboutInfoBlockchain() {
  auto info_blockchain = Blockchain::GetInfo();

  ui->total_blocks_label->setText(
      QString::fromStdString(fmt::format("Total blocks: {}", 0)));  // TODO

  ui->have_blocks_label->setText(QString::fromStdString(
      fmt::format("Have blocks: {}", info_blockchain.total_blocks)));

  ui->connection_peers_label->setText(QString::fromStdString(
      fmt::format("Connection peers: {}", DictionaryOfIP::Get().size())));

  ui->you_root_hash_label->setText(QString::fromStdString(
      fmt::format("You root hash: {}", info_blockchain.root_hash)));

  ui->peers_root_hash_label->setText(
      QString::fromStdString(fmt::format("Peers root hash: {}", "NOT")));

  Network::SendMessageForAllPeers("test message");
}
