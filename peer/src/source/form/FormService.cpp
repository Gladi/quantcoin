// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/form/FormService.hpp"

void FormService::WindowSwitch(QWidget* your_window,
                               QDialog& new_window_for_open) noexcept {
  FormService::GetCenter(new_window_for_open);

  new_window_for_open.activateWindow();
  your_window->hide();

  new_window_for_open.exec();
}

void FormService::GetCenter(QWidget& window) noexcept {
  window.setGeometry(QStyle::alignedRect(
      Qt::LeftToRight, Qt::AlignCenter, window.size(),
      qApp->desktop()
          ->availableGeometry()));  // TODO странное поведение Qt и компилятора
}

void FormService::HideClose(QWidget& window) noexcept {
  window.setWindowFlags(window.windowFlags() & (~Qt::WindowCloseButtonHint));
}

void FormService::HideQuestion(QWidget& window) noexcept {
  window.setWindowFlags(window.windowFlags() &
                        (~Qt::WindowContextHelpButtonHint));
}

void FormService::SetWindowTitle(QWidget* window,
                                 const QString& text,
                                 const QString& name_program) noexcept {
  QString new_title = name_program;
  new_title += ": ";
  new_title += text;

  window->setWindowTitle(new_title);
}

void FormService::SetNoChanged(QWidget* window) noexcept {
  window->setMaximumHeight(window->height());
  window->setMinimumHeight(window->height());

  window->setMaximumWidth(window->width());
  window->setMinimumWidth(window->width());
}
