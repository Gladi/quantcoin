// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/form/Loading/LoadingServiceManager.hpp"

service_manager::vector_iservices_t
LoadingServiceManager::GetServices() noexcept {
  service_manager::vector_iservices_t vector_iservices;

  AddCoreServices(vector_iservices);

  return vector_iservices;
}

void LoadingServiceManager::AddCoreServices(
    service_manager::vector_iservices_t& vector_iservices) noexcept {
  auto services = AllService::GetCoreService();

  for (auto& service : services)
    vector_iservices.push_back(std::move(service));
}
