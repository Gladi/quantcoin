// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/form/Closing/Closing.hpp"
#include "../../../form/ui_Closing.h"

Closing::Closing(QWidget* parent) : QDialog(parent), ui(new Ui::Closing) {
  ui->setupUi(this);
  create_thread();
}

Closing::~Closing() {
  delete ui;
}

void Closing::create_thread() noexcept {
  Blockchain::Close();

  auto services = ClosingServiceManager::GetServices();
  ServiceStopManager service_stop_manager(services);
  service_stop_manager.work();
  auto [output, message_error] = service_stop_manager.get_result();

  if (output == service_manager::critical_error) {
    QString text_for_qmessagebox = "Error closing: ";
    text_for_qmessagebox += QString::fromStdString(message_error);

    QMessageBox::critical(this, compiler_config::NAME_CRYPTOCURRENCY,
                          text_for_qmessagebox, QMessageBox::Ok);
    std::abort();
  } else {
    QApplication::closeAllWindows();
    std::exit(EXIT_SUCCESS);
  }
}
