// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/form/Closing/ClosingServiceManager.hpp"

vector_iservices_t ClosingServiceManager::GetServices() noexcept {
  vector_iservices_t vector_iservices;
  AddCoreService(vector_iservices);

  return vector_iservices;
}

void ClosingServiceManager::AddCoreService(
    vector_iservices_t& services) noexcept {
  auto services_for_add = AllService::GetCoreService();

  for (auto& service : services_for_add)
    if (service->need_to_stop() == true)
      services.push_back(std::move(service));
}
