// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/Service/ServiceStartManager.hpp"
#include "spdlog/spdlog.h"

void ServiceStartManager::start() noexcept {
  // Sorting services
  auto [first_critical_services, user_services, last_critical_services] =
      ServiceStartManager::sorting_services(services);

  // Start all services
  have_critical_error_t for_check_error_first_critical_services =
      start_services(first_critical_services);
  if (check_error(for_check_error_first_critical_services) == true)
    return;

  auto for_check_error_user_services = start_services(user_services);
  if (check_error(for_check_error_user_services) == true)
    return;

  auto for_check_error_last_critical_services =
      start_services(last_critical_services);
  if (check_error(for_check_error_last_critical_services) == true)
    return;

  // Done work
  if (has_warning == false)
    all_work_done(service_manager::successful, "");
  else
    all_work_done(service_manager::successful_but_there_are_problems,
                  "PLEASE, CHECK YOUR SERVICES");
}

std::tuple<ServiceStartManager::first_critical_services_t,
           ServiceStartManager::user_services_t,
           ServiceStartManager::last_critical_services_t>
ServiceStartManager::sorting_services(
    vector_iservices_t& no_sort_services) noexcept {
  ServiceStartManager::first_critical_services_t first_critical;
  ServiceStartManager::user_services_t user;
  ServiceStartManager::last_critical_services_t last_critical;

  for (auto&& service : no_sort_services) {
    switch (service->get_type()) {
      case IService::critical_first:
        first_critical.push_back(std::move(service));
        break;
      case IService::user:
        user.push_back(std::move(service));
        break;
      case IService::critical_last:
        last_critical.push_back(std::move(service));
        break;
    }
  }

  return {std::move(first_critical), std::move(user), std::move(last_critical)};
}

have_critical_error_t ServiceStartManager::start_services(
    const vector_iservices_t& services) noexcept {  //-V688
  boost_asio::thread_pool thread_pool(std::thread::hardware_concurrency());
  qstd::mutex_object<have_critical_error_t> have_critical_error(
      create_have_critical_error());  // Services can be multithreaded

  for (auto&& service : services) {
    switch (service->get_properties()) {
      case IService::no_thread:
        start_one_service(service, thread_pool, have_critical_error);
        done_one_service();
        break;

      default:  // IService::normal
        boost_asio::post(thread_pool, [&]() {
          start_one_service(service, thread_pool, have_critical_error);
          done_one_service();
        });
        break;
    }
  }

  thread_pool.join();
  thread_pool.wait();

  return have_critical_error.copy();
}

void ServiceStartManager::start_one_service(
    const iservice_ptr& service,
    boost_asio::thread_pool& thread_pool,
    qstd::mutex_object<have_critical_error_t>& have_critical_error) noexcept {
  auto status_service = service->start();

  switch (status_service) {
    case IService::error: {
      thread_pool.stop();
      have_critical_error.swap(create_have_critical_error(
          true, service->get_message_error()));  //-V509
      break;
    }
    case IService::warning:
      has_warning = true;
      break;
    case IService::done:
      spdlog::debug("Service start done: {}",
                    service->get_message_successful());
      break;
  }
}
