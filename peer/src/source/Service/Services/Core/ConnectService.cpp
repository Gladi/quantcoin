// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/Service/Services/Core/ConnectService.hpp"

IService::type_output_work ConnectService::stop() {
  return IService::done;
}

IService::type_service ConnectService::get_type() {
  return IService::critical_first;
}

std::string ConnectService::get_message_error() {
  return "ERROR CONNECTION TO TRACKER: " + description_error;
}

std::string ConnectService::get_message_successful() {
  return "DONE CONNECTION TO TRACKER";
}

IService::type_output_work ConnectService::start() {
  try {
    Network::Connect(compiler_config::ADDRESS_TO_TRACKER,
                     compiler_config::PORT);
  } catch (const std::exception& ex) {
    description_error = ex.what();
    return IService::error;
  }

  return IService::done;
}

IService::properties ConnectService::get_properties() {
  return IService::no_thread;
}

bool ConnectService::need_to_stop() {
  return true;
}
