#include "header/Service/Services/Core/ServerService.hpp"

std::string ServerService::get_message_successful() {
  return "Server Service: start successful";
}

IService::type_output_work ServerService::start() {
  auto [ip, _port] = DictionaryOfIP::GetMyInfo();
  if (_port == "" || _port == "0")
    abort();

  server_thread = new std::thread([=]() {
    spdlog::debug("Start server_thread _port = {}", _port);
    Server::start(atoi(_port.c_str()));
  });  // TODO
  server_thread->detach();

  return type_output_work::done;
}

std::string ServerService::get_message_error() {
  return "";
}

IService::properties ServerService::get_properties() {
  return properties::normal;
}

IService::type_service ServerService::get_type() {
  return type_service::critical_last;
}

bool ServerService::need_to_stop() {
  return true;
}

IService::type_output_work ServerService::stop() {
  // server_thread->join();
  // delete server_thread;
  return type_output_work::done;
}
