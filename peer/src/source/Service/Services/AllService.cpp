// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/Service/Services/AllService.hpp"

vector_iservices_t AllService::GetCoreService() noexcept {
  vector_iservices_t vector_iservices;

  ConnectService connect_service;
  ServerService server_service;

  add_iservice_to_vector<ConnectService>(vector_iservices, connect_service);
  add_iservice_to_vector<ServerService>(vector_iservices, server_service);
  // vector_iservices.push_back(std::make_unique<ServerService>(server_service));

  return vector_iservices;
}
