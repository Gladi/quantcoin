// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/Service/ServiceStopManager.hpp"
#include "spdlog/spdlog.h"

void ServiceStopManager::work() noexcept {
  boost_asio::thread_pool thread_pool(std::thread::hardware_concurrency());

  boost_asio::post(thread_pool, [&]() {
    for (auto&& service : services) {
      auto output = service->stop();

      switch (output) {
        case IService::done:
          spdlog::debug("Service stop done: {}",
                        service->get_message_successful());
          break;
        case IService::warning:
          have_warning = true;
          break;
        case IService::error:
          have_critical_error.swap(
              create_have_critical_error(true, service->get_message_error()));
          break;
      }
    };
  });

  thread_pool.join();
  thread_pool.wait();
}

ServiceStopManager::stop_services_output_t
ServiceStopManager::get_result() noexcept {
  auto [have_error, message_error] = *have_critical_error;

  if (have_error == true)
    return std::make_tuple(service_manager::critical_error, message_error);
  else if (have_warning == true)
    return std::make_tuple(service_manager::successful_but_there_are_problems,
                           "");

  return std::make_tuple(service_manager::successful, "");
}
