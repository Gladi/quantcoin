// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/Network/Network.hpp"

QSocket Network::socket;
std::mutex Network::mutex;

void Network::Connect(const std::string& host_address, uint16_t port) {
  std::scoped_lock<std::mutex> lock(mutex);

  auto [public_key, private_key] = Crypto::RSA_API::GenerateRSA();
  RSASingleton::SetPublicKey(public_key);
  RSASingleton::SetPrivateKey(private_key);

  ConnectionPhases connection_phases(socket);
  connection_phases.connecting_to_tracker(host_address, port);
  spdlog::debug("ConnectionPhases: connecting_to_tracker done");
  connection_phases.check_version();
  spdlog::debug("ConnectionPhases: check_version done");
  connection_phases.get_dictionary_of_ip();
  spdlog::debug("ConnectionPhases: get_dictionary_of_ip done");
  connection_phases.add_dictionary_of_ip();
  spdlog::debug("ConnectionPhases: add_dictionary_of_ip done");
}

QSocket& Network::GetSocket() {
  std::scoped_lock<std::mutex> lock(mutex);
  return socket;
}

void Network::Disconnect() {
  std::scoped_lock<std::mutex> lock(mutex);
  socket.disconnect();
}

void Network::SendMessageForAllPeers(const std::string& message) {
  std::scoped_lock<std::mutex> lock(mutex);

  auto message_for_socket =
      quantcoin_api::builder::SEND_MESSAGE_FOR_ALL_PEERS(message);
  spdlog::info("SendMessageForAllPeers message = {}", message);
  socket.send(message_for_socket);
}
