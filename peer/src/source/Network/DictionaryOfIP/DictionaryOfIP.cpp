// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com
#include "header/Network/DictionaryOfIP/DictionaryOfIP.hpp"

dictionary_of_ip_t DictionaryOfIP::data;
std::mutex DictionaryOfIP::mutex;
info_peer_t DictionaryOfIP::info_peer;

void DictionaryOfIP::Download(QSocket& socket) {
  std::scoped_lock<std::mutex> lock(mutex);
  data = DictionaryOfIPService::Get(socket);
}

dictionary_of_ip_t DictionaryOfIP::Get() {
  std::scoped_lock<std::mutex> lock(mutex);
  return data;
}

bool DictionaryOfIP::IsEmpty() {
  std::scoped_lock<std::mutex> lock(mutex);
  return data.empty();
}

void DictionaryOfIP::SetMyInfo(const info_peer_t& info) {
  std::scoped_lock<std::mutex> lock(mutex);
  info_peer = info;
}

info_peer_t DictionaryOfIP::GetMyInfo() {
  std::scoped_lock<std::mutex> lock(mutex);
  return info_peer;
}

info_peer_t DictionaryOfIP::GetRandomPeer() noexcept {
  std::scoped_lock<std::mutex> lock(mutex);
  if (data.size() == 0)
    return {};

  dictionary_of_ip_t result;
  std::ranges::sample(data, std::back_inserter(result), 1,
                      std::mt19937{std::random_device{}()});

  return result[0];
}
