// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include <cassert>
#include "header/Network/DictionaryOfIP/DictionaryOfIPService.hpp"

dictionary_of_ip_t DictionaryOfIPService::Get(QSocket& socket,
                                              const size_t& size_for_get_ips) {
  dictionary_of_ip_t dictionary_of_ip;  //-V808

  socket.send(quantcoin_api::builder::GET_DICTIONARY_OF_IP(size_for_get_ips));
  auto message = socket.receive(
      quantcoin_api::detail::get_dictionary::SIZE_BUFFER_FOR_ONE_IP_PAIR *
      size_for_get_ips);

  if (message == quantcoin_api::macros::NO_IP_FOR_DICTIONARY)
    return dictionary_of_ip;

  return quantcoin_api::builder::PARSER_DICTIONARY_OF_IP(message);
}
