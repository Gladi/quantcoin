#include "header/Network/Server/Server.hpp"

SocketReactor Server::reactor;

void Server::start(const int& port) {
  try {
    ServerSocket svs(port);
    ParallelSocketAcceptor<ServerLoop, SocketReactor> acceptor(svs, reactor);
    spdlog::info("Start server done: port = {}", port);
    reactor.run();
  } catch (...) {
    spdlog::critical("Start server error: port = {}", port);
    std::abort();
  }
}
