#include "header/Network/Server/ServerHandler.hpp"

std::mutex ServerHandler::mutex;
std::function<void()> ServerHandler::FinishGetBlocks;

void ServerHandler::SetFinishGetBlocks(std::function<void()> handler) {
  std::scoped_lock<std::mutex> lock(mutex);
  FinishGetBlocks = handler;
}

void ServerHandler::RunFinishGetBlocks() {
  std::scoped_lock<std::mutex> lock(mutex);
  FinishGetBlocks();
}

void ServerHandler::SetStartGetBlocksForOtherPeer(
    std::function<void()> handler) {
  std::scoped_lock<std::mutex> lock(mutex);
  StartGetBlocksForOtherPeer = handler;
}

void ServerHandler::RunStartGetBlocksForOtherPeer() {
  std::scoped_lock<std::mutex> lock(mutex);
  StartGetBlocksForOtherPeer();
}
