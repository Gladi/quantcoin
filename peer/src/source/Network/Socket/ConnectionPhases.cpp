// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/Network/Socket/ConnectionPhases.hpp"

void ConnectionPhases::connecting_to_tracker(const std::string& host_address,
                                             uint16_t port) {
  try {
    socket.connect(host_address, port);
  } catch (...) {
    throw NetworkError(NetworkError::Type::connecting);
  }
}

void ConnectionPhases::check_version() {
  try {
    socket.send(quantcoin_api::macros::GET_VERSION);

    auto message = socket.receive();
    if (message != compiler_config::VERSION)
      throw NetworkError(NetworkError::Type::version_is_old);
  } catch (const Poco::Error& error) {  // Otherwise, the program will crash
    throw NetworkError(NetworkError::Type::socket);
  }
}

void ConnectionPhases::add_dictionary_of_ip() {
  try {
    uint8_t attempts = 10;

    // UPnP::RemovePort<UPnP::TypeProtocol::UDP>();
    // UPnP::RemovePort<UPnP::TypeProtocol::TCP>();
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(4000, 7000);

    auto port_for_all = dist6(rng);
    spdlog::debug(
        "ConnectionPhases::add_dictionary_of_ip random port_for_all = {}",
        port_for_all);
    volatile auto upnp_info = UPnP::OpenPort<UPnP::TypeProtocol::TCP>(
        port_for_all);  // TODO в роутере отличается адреса

    auto port =
        qstd::convert<uint16_t, std::string>(upnp_info.mapped_public_port);
    auto ip = HTML::GetContent(HTML::useful_links::GET_IP_ADDRESS);

    spdlog::debug("port = {}, upnp_info.mapped_public_port = {}", port,
                  upnp_info.mapped_public_port);
    spdlog::debug("upnp_info: private_port = {}, public_port = {}",
                  upnp_info.private_port, upnp_info.mapped_public_port);

    if (port == "0" || upnp_info.mapped_public_port == 0 ||
        upnp_info.private_port == 0)  // Retry
      if (attempts != 0) {
        add_dictionary_of_ip();
        --attempts;
      } else
        throw NetworkError(NetworkError::Type::add_dictionary_of_ip);

    socket.send(quantcoin_api::builder::ADD_DICTIONARY_OF_IP(
        ip, qstd::convert<uint16_t, std::string>(port_for_all)));

    if (socket.receive() != "Y") {
      spdlog::debug("add_dictionary_of_ip socket.receive() == N; retry");
      add_dictionary_of_ip();  // Retry
    }

    DictionaryOfIP::SetMyInfo(
        std::make_tuple(ip, std::to_string(upnp_info.mapped_public_port)));
  } catch (const Poco::Error&) {
    throw NetworkError(NetworkError::Type::add_dictionary_of_ip);
  } catch (const HTMLError&) {
    throw NetworkError(NetworkError::Type::add_dictionary_of_ip);
  }
}

void ConnectionPhases::get_dictionary_of_ip() {
  try {
    DictionaryOfIP::Download(socket);
    spdlog::debug("ConnectionPhases::get_dictionary_of_ip result = {}",
                  DictionaryOfIP::Get().size());

    for (auto temp = DictionaryOfIP::Get();
         const auto& [ip_address, port] : temp)
      spdlog::info("ip address = {}, port = {}", ip_address, port);
  } catch (const Poco::Error& error) {
    throw NetworkError(NetworkError::Type::get_dictionary_of_ip);
  }
}
