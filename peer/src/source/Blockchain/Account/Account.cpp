// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com
#include "header/Blockchain/Account/Account.hpp"

Account AccountService::Create(const unsigned int& size_key) {
  const auto [public_key, private_key] = Crypto::RSA_API::GenerateRSA(size_key);
  const auto random_hash = Hash::GenerateRandomSHA1();

  return Account{.wallet_address = random_hash,
                 .public_key = public_key,
                 .private_key = private_key};
}

std::string AccountService::GetString(const Account& account) {
  return fmt::format("{}|{}|{}", account.wallet_address, account.public_key,
                     account.private_key);
}

Account AccountService::ParserString(const std::string& text) {
  auto args = quantcoin_api::detail::split(text, '|');

  return Account{
      .wallet_address = args[0], .public_key = args[1], .private_key = args[2]};
}
