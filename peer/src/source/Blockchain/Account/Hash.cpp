// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com
#include "header/Security/Hash.hpp"

std::string Hash::GenerateRandomString(const size_t& len) {
  static const char alphanum[] =
      "0123456789"
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
      "abcdefghijklmnopqrstuvwxyz";

  std::string tmp_s;
  tmp_s.reserve(len);

  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> dist6(
      0, sizeof(alphanum));
  for (size_t i = 0; i < len; ++i) {
    tmp_s += alphanum[dist6(rng)];
  }

  return tmp_s;
}

void Hash::simpleSHA1(unsigned char* input,
                      unsigned long length,
                      unsigned char* md) {
  md = SHA1(input, length, md);
}

std::string Hash::GenerateRandomSHA1(const size_t& length_for_random) {
  constexpr size_t SIZE_FOR_HASH = SHA_DIGEST_LENGTH;

  unsigned char md[SIZE_FOR_HASH];
  auto input = GenerateRandomString(length_for_random);
  simpleSHA1(reinterpret_cast<unsigned char*>(input.data()), SIZE_FOR_HASH, md);

  return ConvertHashToString<SIZE_FOR_HASH>(md);
}

std::string Hash::GenerateSHA256(std::string text) {
  unsigned char md[SHA256_DIGEST_LENGTH];
  SHA256(reinterpret_cast<unsigned char*>(text.data()), SHA256_DIGEST_LENGTH,
         md);

  return ConvertHashToString<SHA256_DIGEST_LENGTH>(md);
}
