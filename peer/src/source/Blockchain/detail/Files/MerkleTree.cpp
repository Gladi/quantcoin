#include "header/Blockchain/detail/Files/MerkleTree.hpp"
#include "spdlog/spdlog.h"

void MerkleTree::add(const std::string& hash) noexcept {
  for_work.push_back(hash);
}

std::string MerkleTree::get_root() {
  // Check on error
  if (for_work.size() == 1)
    return Hash::GenerateSHA256(for_work[0]);

  if (for_work.size() == 2)
    return hashing(for_work[0], for_work[1]);

  if (for_work.empty())
    throw MerkleTreeException(
        MerkleTreeException::Type::NotFoundTextForHashing);

  std::unique_ptr<std::string> extra_node;
  std::vector<std::string> nodes = for_work;

work:
  check_on_extra_node(extra_node, nodes);

  nodes = get_nodes(nodes);

  if (nodes.size() != 1)
    goto work;  // Retry

  if (!extra_node)
    return nodes[0];
  else
    return hashing(nodes[0], *extra_node);
}

std::vector<std::string> MerkleTree::get_nodes(
    std::vector<std::string>& nodes) {
  std::vector<std::tuple<std::string, std::string>> for_hashing;
  auto [half1, half2] = split_vector_in_half<std::string>(nodes);

  for (size_t i = 0; i < half1.size(); ++i) {
    spdlog::info("MerkleTree::get_nodes: half1 = {}, half2 = {}", half1[i],
                 half2[i]);
    for_hashing.push_back(std::make_tuple(half1[i], half2[i]));
  }

  return get_hash(for_hashing);
}
std::vector<std::string> MerkleTree::get_hash(
    std::vector<std::tuple<std::string, std::string>> for_hashing) {
  std::vector<std::string> result;
  for (auto& [node1, node2] : for_hashing) {
    spdlog::info("MerkleTree::get_hash node1: {}; node2: {}", node1, node2);
    result.push_back(hashing(node1, node2));
  }

  for (auto& i : result)
    spdlog::info("MerkleTree::get_hash result: {}", i);

  return result;
}
