// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com
#include "header/Blockchain/LevelDB/LevelDB.hpp"

void LevelDB::open(const std::string& path) {
  leveldb::Options options;
  options.create_if_missing = true;
  options.compression = leveldb::kSnappyCompression;
  options.paranoid_checks = true;

  const auto status = leveldb::DB::Open(options, path, &db);
  this->path = path;

  if (status.ok() == false)
    throw LevelDBException(LevelDBException::Type::open);

  _is_open = true;
}

void LevelDB::write(const std::string& key, std::string value) {
  auto options = leveldb::WriteOptions();
  options.sync = true;

  const auto status = db->Put(options, key, value);

  if (status.ok() == false)
    throw LevelDBException(LevelDBException::Type::write);
}

std::string LevelDB::read(const std::string& key) {
  std::string value;
  auto options = leveldb::ReadOptions();
  options.verify_checksums = true;

  auto status = db->Get(options, key, &value);

  if (status.ok() == false)
    throw LevelDBException(LevelDBException::Type::read);

  return value;
}

bool LevelDB::is_exists(const std::string& key) {
  std::string value;

  const auto status = db->Get(leveldb::ReadOptions(), key, &value);
  return !(status.IsNotFound());
}

LevelDB::~LevelDB() noexcept {
  if (_is_open)
    delete db;
}

void LevelDB::close() {
  if (_is_open)
    delete db;

  _is_open = false;
}

std::string LevelDB::get_path() noexcept {
  return path;
}

bool LevelDB::is_open() noexcept {
  return _is_open;
}

uint64_t LevelDB::get_balance(const std::string& wallet_address) {
  auto options = leveldb::ReadOptions();
  options.verify_checksums = true;
  int64_t balance = 0;

  auto idb = db->NewIterator(options);
  for (idb->SeekToFirst(); idb->Valid(); idb->Next()) {
    std::string key = idb->key().ToString();
    auto args = quantcoin_api::detail::split(key, '|');

    if (args[0] == "SEND_COINS" && args[1] == wallet_address)
      balance -= atoll(args[3].c_str());
    else if (args[0] == "SEND_COINS" && args[2] == wallet_address)
      balance += atoll(args[3].c_str());
  }

  return balance;
}
