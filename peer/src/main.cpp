// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include <QApplication>
#include "spdlog/spdlog.h"

#include "header/Service/Libs/CleanLibs.hpp"
#include "header/Service/Libs/InitLibs.hpp"
#include "header/form/Loading/Loading.hpp"

inline void init_libs() {
  try {
    InitLibs::All();
  } catch (const std::exception& ex) {
    spdlog::critical("ERROR LOADING LIBS: {}", ex.what());
    std::abort();
  }
};

static inline void clean_libs() {
  try {
    InitLibs::All();
  } catch (const std::exception& ex) {
    spdlog::critical("ERROR LOADING LIBS: {}", ex.what());
    std::abort();
  }
};

//! Start Quantcoin
int main(int argc, char* argv[]) {
  init_libs();
  std::set_terminate([]() {
    std::cerr << "terminate called after throwing an instance of ";
    try {
      std::rethrow_exception(std::current_exception());
    } catch (const std::exception& ex) {
      std::cerr << typeid(ex).name() << std::endl;
      std::cerr << "  what(): " << ex.what() << std::endl;
    } catch (...) {
      std::cerr << typeid(std::current_exception()).name() << std::endl;
      std::cerr << " ...something, not an exception, dunno what." << std::endl;
    }
    std::cerr << "errno: " << errno << ": " << std::strerror(errno)
              << std::endl;
    std::abort();
  });

  QApplication a(argc, argv);
  spdlog::set_level(spdlog::level::debug);

  spdlog::debug("IP ADDRESS: {}", HTML::GetContent("http://ifconfig.me"));

  Loading w;
  w.show();

  CleanLibs::All();  // TODO перенести в Closing
  return a.exec();
}
