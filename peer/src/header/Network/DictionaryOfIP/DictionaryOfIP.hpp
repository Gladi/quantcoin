#pragma once

#include <mutex>
#include <random>
#include <ranges>

#include "DictionaryOfIPService.hpp"

struct DictionaryOfIP {
  static void Download(QSocket& socket);

  static void SetMyInfo(const info_peer_t& info);

  static dictionary_of_ip_t Get();

  static bool IsEmpty();

  static info_peer_t GetMyInfo();

  static info_peer_t GetRandomPeer() noexcept;

 private:
  static std::mutex mutex;
  static dictionary_of_ip_t data;
  static info_peer_t info_peer;
};
