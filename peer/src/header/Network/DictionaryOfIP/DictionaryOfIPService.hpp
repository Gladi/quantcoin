#pragma once

#include <variant>
#include "header/QSTL/tools/convert.hpp"

#include "../../../../../api/Network/TrackerAPI.hpp"
#include "DictionaryOfIPTypes.hpp"
#include "header/Network/Socket/QSocket.hpp"

struct DictionaryOfIPService {
  //! Get Dictionary Of IP
  //! \return If dictionary_of_ip_t.size() == 0, this NO_IP_FOR_DICTIONARY
  static dictionary_of_ip_t Get(QSocket& socket,
                                const size_t& size_for_get_ips =
                                    compiler_config::SIZE_IPS_FOR_DICTIONARY);
};
