#pragma once

#include <exception>

struct NetworkError : std::exception {
  enum class Type {
    connecting,
    version_is_old,
    socket,
    get_dictionary_of_ip,
    add_dictionary_of_ip
  };

  explicit NetworkError(NetworkError::Type type_error) noexcept
      : type_error(type_error){};

  [[nodiscard]] const char* what() const noexcept override;

  NetworkError::Type type_error;
};