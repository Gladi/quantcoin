#pragma once

#include <exception>
#include <mutex>
#include <thread>
#include "spdlog/spdlog.h"

#include "Socket/ConnectionPhases.hpp"
#include "header/Security/Crypto.hpp"

struct Network {
 private:
  Network() = delete;
  ~Network() = delete;

  static QSocket socket;
  static std::mutex mutex;

 public:
  static void Connect(const std::string& host_address, uint16_t port);

  static void Disconnect();

  static QSocket& GetSocket();

  static void SendMessageForAllPeers(const std::string& message);
};
