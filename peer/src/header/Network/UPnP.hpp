#pragma once

#include <exception>
#include "../../libs/libnatpmp/natpmp.h"
#include "config.hpp"
#include "for_upnp.h"

namespace UPnP {

enum class TypeProtocol { TCP, UDP };

namespace detail {

enum class UPnPTypeError { init_natpmp, send_new_port_mapping_request };

template <TypeProtocol TypeProtocol>
void add_port_mapping_example(upnp::igd& igd,
                              uint16_t ext_p,
                              uint16_t int_p,
                              net::yield_context yield) {
  if constexpr (TypeProtocol == TypeProtocol::UDP) {
    auto r = igd.add_port_mapping(upnp::igd::protocol::udp, ext_p, int_p,
                                  compiler_config::NAME_CRYPTOCURRENCY,
                                  chrono::minutes(1), yield);
    if (!r)
      throw 1;
  } else if constexpr (TypeProtocol == TypeProtocol::TCP) {
    auto r = igd.add_port_mapping(upnp::igd::protocol::tcp, ext_p, int_p,
                                  compiler_config::NAME_CRYPTOCURRENCY,
                                  chrono::minutes(1), yield);
    if (!r)
      throw 1;
  }
}

};  // namespace detail

struct info_about_opened_port {
  uint16_t private_port;
  uint16_t mapped_public_port;
  uint32_t lifetime;
};

struct UPnPError : std::exception {
  UPnPError(const detail::UPnPTypeError& type_error) noexcept
      : type_error(type_error){};

  [[nodiscard]] const char* what() const noexcept override {
    switch (type_error) {
      case detail::UPnPTypeError::init_natpmp:
        return "UPnP Error: init_natpmp";
      case detail::UPnPTypeError::send_new_port_mapping_request:
        return "UPnP Error: send_new_port_mapping_request";
      default:
        return "UPnP Error: Unknown error";
    }
  };

  detail::UPnPTypeError type_error;
};

#include <errno.h>

template <TypeProtocol TypeProtocol>
static void RemovePort() {
  natpmp_t natpmp;
  int result;
  if (initnatpmp(&natpmp, 0, 0) != 0)
    throw UPnPError(detail::UPnPTypeError::init_natpmp);

  if constexpr (TypeProtocol == TypeProtocol::TCP) {
    result = sendnewportmappingrequest(&natpmp, NATPMP_PROTOCOL_TCP, 0, 0, 0);
    if (result < 0) {
      throw UPnPError(detail::UPnPTypeError::send_new_port_mapping_request);
    }
  } else if (TypeProtocol == TypeProtocol::UDP) {
    result = sendnewportmappingrequest(&natpmp, NATPMP_PROTOCOL_UDP, 0, 0, 0);
    if (result != 12) {
      throw UPnPError(detail::UPnPTypeError::send_new_port_mapping_request);
    }
  }

  int r = 0;
  natpmpresp_t response;
  do {
    fd_set fds;
    struct timeval timeout;
    FD_ZERO(&fds);
    FD_SET(natpmp.s, &fds);
    getnatpmprequesttimeout(&natpmp, &timeout);
    select(FD_SETSIZE, &fds, NULL, NULL, &timeout);
    r = readnatpmpresponseorretry(&natpmp, &response);
  } while (r == NATPMP_TRYAGAIN);

  // if (r != 0) {
  //    throw UPnPError(detail::UPnPTypeError::send_new_port_mapping_request);
  //    TODO
  //}

  closenatpmp(&natpmp);
}

template <TypeProtocol TypeProtocol>
static info_about_opened_port OpenPort(const uint16_t& port) {
  uint16_t result_public_port = 0;
  net::io_context ctx;

  net::spawn(ctx, [&](net::yield_context yield) {
    auto r_igds = upnp::igd::discover(ctx.get_executor(), yield);

    if (!r_igds)
      throw 1;

    auto igds = move(r_igds.value());

    net::ip::udp::socket socket(
        ctx, net::ip::udp::endpoint(net::ip::address_v4::any(), 0));

    for (auto& igd : igds) {
      result_public_port = socket.local_endpoint().port();
      detail::add_port_mapping_example<TypeProtocol>(
          igd, port, socket.local_endpoint().port(), yield);
    }
  });

  ctx.run();

  info_about_opened_port info{
      .private_port = port,  // TODO Возможно надо наоборот
      .mapped_public_port = result_public_port,
      .lifetime = 3000,
  };

  return info;
};
};  // namespace UPnP
