#pragma once

#include <functional>
#include <mutex>

struct ServerHandler {
  static void SetFinishGetBlocks(std::function<void()> handler);
  static void RunFinishGetBlocks();

  static void SetStartGetBlocksForOtherPeer(std::function<void()> handler);
  static void RunStartGetBlocksForOtherPeer();

 private:
  static std::mutex mutex;
  static std::function<void()> FinishGetBlocks;
  static std::function<void()>
      StartGetBlocksForOtherPeer;  // TODO Использовать!
};
