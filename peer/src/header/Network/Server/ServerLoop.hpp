#pragma once

#include <string>
#include <thread>
#include "../../api/Network/TrackerAPI.hpp"
#include "Poco/Net/SocketNotification.h"
#include "Poco/Net/SocketReactor.h"
#include "Poco/Net/StreamSocket.h"
#include "fmt/format.h"
#include "header/Security/Crypto.hpp"
#include "spdlog/spdlog.h"

using Poco::Observer;
using Poco::Net::ReadableNotification;
using Poco::Net::SocketAddress;
using Poco::Net::SocketReactor;
using Poco::Net::StreamSocket;

namespace ServerLoopCommand {

static constexpr const char* const SEND_INFO_ABOUT_BLOCKCHAIN =
    "SEND_INFO_ABOUT_BLOCKCHAIN";

static constexpr const char* const SEND_BLOCK_IN_BLOCKCHAIN =
    "SEND_BLOCK_IN_BLOCKCHAIN";

static constexpr const char* const SEND_COINS = "SEND_COINS";

static constexpr const char* const ADD_NEW_ACCOUNT = "ADD_NEW_ACCOUNT";

};  // namespace ServerLoopCommand

struct ServerCommand {
#define ARGS_SERVER_LOOP StreamSocket &socket, const std::string &message

#define COMMAND_SERVER_LOOP(name) static void name(ARGS_SERVER_LOOP)

  static void Run(ARGS_SERVER_LOOP);

 private:
  COMMAND_SERVER_LOOP(SendInfoAboutBlockchain);

  COMMAND_SERVER_LOOP(SendBlockInBlockchain);

  COMMAND_SERVER_LOOP(SendCoins);

  COMMAND_SERVER_LOOP(AddNewAccount);

  COMMAND_SERVER_LOOP(GetBlock);
};

class ServerLoop {
 public:
  ServerLoop(StreamSocket& socket, SocketReactor& reactor)
      : _socket(socket), _reactor(reactor) {
    spdlog::info("New connection Server Loop: ip = {}",
                 socket.peerAddress().toString());

    _reactor.addEventHandler(_socket,
                             Observer<ServerLoop, ReadableNotification>(
                                 *this, &ServerLoop::onReadable));
    spdlog::debug("ServerLoop new connection!");
  }

  ~ServerLoop();

  void onReadable(ReadableNotification* pNf);

 private:
  StreamSocket _socket;
  SocketReactor& _reactor;
  std::mutex mutex;
};
