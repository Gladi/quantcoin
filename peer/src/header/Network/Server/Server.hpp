#pragma once

#include "Poco/Net/ParallelSocketAcceptor.h"

#include "ServerLoop.hpp"
#include "config.hpp"
#include "header/Network/DictionaryOfIP/DictionaryOfIP.hpp"

using Poco::Net::ParallelSocketAcceptor;

struct Server {  // TODO первести это в TCP и использовать наш Socket
  static constexpr auto SIZE_BUFFER = compiler_config::SIZE_BUFFER_FOR_SOCKET;

  explicit Server() = default;
  Server(const Server&) = delete;
  Server& operator=(const Server&) = delete;
  ~Server() = default;

  static void start(const int& port);

 private:
  static SocketReactor reactor;
};
