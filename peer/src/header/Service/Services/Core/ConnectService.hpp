#pragma once

#include "header/Network/Network.hpp"
#include "header/Service/Services/IService.hpp"

//! Services for connect to tracker
class ConnectService final : public IService {
 public:
  ConnectService() = default;
  ~ConnectService() = default;

  IService::type_output_work stop() override;

  IService::type_service get_type() override;

  std::string get_message_error() override;

  std::string get_message_successful() override;

  IService::type_output_work start() override;

  IService::properties get_properties() override;

  bool need_to_stop() override;

 private:
  std::string description_error;
};
