//
// Created by gladi on 27.11.2021.
//

#pragma once

#include <thread>
#include <tuple>
#include "boost/asio/post.hpp"
#include "boost/asio/thread_pool.hpp"

#include "header/QSTL/tools/mutex_object.hpp"
#include "header/QSTL/types/help_types.hpp"
#include "service_manager.hpp"

namespace boost_asio = boost::asio;
namespace sboost = boost::signals2;
using namespace service_manager;

//! For start services
class ServiceStartManager final {
 public:
  explicit ServiceStartManager(
      vector_iservices_t& services,
      sboost::signal<void()>& done_one_service,
      signal_for_all_work_done_t& all_work_done) noexcept
      : services(services),
        done_one_service(done_one_service),
        all_work_done(all_work_done){};
  ~ServiceStartManager() = default;

  void start() noexcept;

 private:
  using first_critical_services_t = service_manager::vector_iservices_t;
  using user_services_t = service_manager::vector_iservices_t;
  using last_critical_services_t = service_manager::vector_iservices_t;
  [[nodiscard]] std::tuple<first_critical_services_t,
                           user_services_t,
                           last_critical_services_t>
  sorting_services(vector_iservices_t& no_sort_services) noexcept;

  [[nodiscard]] have_critical_error_t start_services(
      const vector_iservices_t& services) noexcept;
  void start_one_service(
      const iservice_ptr& service,
      boost_asio::thread_pool& thread_pool,
      qstd::mutex_object<have_critical_error_t>& have_critical_error) noexcept;

  vector_iservices_t& services;
  sboost::signal<void()>& done_one_service;

  signal_for_all_work_done_t& all_work_done;
  bool has_warning = false;

  inline bool check_error(
      const have_critical_error_t& critical_error_for_check) noexcept {
    auto [have_critical_error, message_error] = critical_error_for_check;

    if (have_critical_error == true)
      all_work_done(service_manager::critical_error, message_error);

    return have_critical_error;
  };
};
