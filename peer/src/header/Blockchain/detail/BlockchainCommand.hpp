#pragma once

#include <string>
#include "../../../../../api/Network/TrackerAPI.hpp"
#include "fmt/format.h"

#include "header/Blockchain/Account/Account.hpp"

using for_key_t = std::string;
using for_value_t = std::string;

namespace BlockchainCommand {

namespace macros {

static constexpr const char* const ADD_ACCOUNT = "ADD_ACCOUNT";

};

namespace builder {

static inline std::tuple<for_key_t, for_value_t> ADD_ACCOUNT(
    const Account& account) {
  return {account.wallet_address, account.public_key};
};

static inline Account PARSER_ACCOUNT(const std::string& text) {
  auto args = quantcoin_api::detail::split(text, '|');

  return {
      .wallet_address = args[0], .public_key = args[1], .private_key = nullptr};
};

};  // namespace builder

};  // namespace BlockchainCommand
