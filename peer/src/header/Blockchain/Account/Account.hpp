#pragma once

#include "../../../../../api/Network/TrackerAPI.hpp"
#include "fmt/format.h"

#include "header/Security/Crypto.hpp"
#include "header/Security/Hash.hpp"

static constexpr const char* const ACCOUNT_NOT_VALUE = "NO_VALUE";

//! Info about account
//! \warning Please check to ACCOUNT_NOT_VALUE
struct Account {
  std::string wallet_address;
  public_key_t public_key;
  private_key_t private_key;
};

struct AccountService {
  static Account Create(
      const unsigned int& size_key = compiler_config::SIZE_RSA_KEY);

  static std::string GetString(const Account& account);

  static Account ParserString(const std::string& text);
};
