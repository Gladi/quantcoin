#pragma once

#include <exception>

struct AccountError : std::exception {
  enum class Type {
    GetPublicAndPrivateKeyFile,
  };

  AccountError(Type type_error) noexcept : type_error(type_error) {};

  [[nodiscard]] const char* what() const noexcept override {
    switch (type_error) {
      case Type::GetPublicAndPrivateKeyFile:
        return "RSA error: Get public and private key file";
      default:
        return "Account error: Unknown error";
    }
  };

  Type type_error;
};