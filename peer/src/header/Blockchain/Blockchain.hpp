#pragma once

#include <mutex>
#include <utility>
#include "fmt/format.h"

#include "LevelDB/LevelDB.hpp"
#include "LevelDB/LevelDBGuard.hpp"
#include "config.hpp"
#include "detail/Block.hpp"
#include "header/Blockchain/Account/Account.hpp"
#include "header/Blockchain/detail/BlockchainCommand.hpp"
#include "header/Network/Network.hpp"
#include "header/Network/Server/Server.hpp"
#include "header/Network/Server/ServerHandler.hpp"

struct BlockchainInfo {
  std::string root_hash;
  size_t total_blocks;
};

struct InfoBlockchainInOtherPeer {
  std::string ip_address;
  int port;
  uint64_t count_blocks;
  std::string root_hash;
};

struct Blockchain {
  static void Open(
      const std::string& path = compiler_config::PATH_TO_BLOCKCHAIN);

  static bool SendCoins(const std::string& to,
                        const std::string& from,
                        const uint64_t& balance);

  static Account CreateAccount();

  static Account GetAccount(const std::string& wallet_address);

  static void Sync();

  static void Close();

  static bool IsCurrent();

  [[nodiscard]] static BlockchainInfo GetInfo();

 private:
  Blockchain() = delete;
  ~Blockchain() = delete;

  struct cache_info {
    BlockchainInfo info;
    bool is_changed = true;
  };

  static std::vector<InfoBlockchainInOtherPeer> GetInfoBlockchainInOtherPeer();
  static LevelDB db;
  static std::mutex mutex;
  static cache_info cache;
};
