#pragma once

#include "LevelDB.hpp"

struct LevelDBGuard final {
  explicit LevelDBGuard(LevelDB& db) noexcept : db(db) { db.close(); };

  ~LevelDBGuard() noexcept { db.open(db.get_path()); };

 private:
  LevelDB& db;
};
