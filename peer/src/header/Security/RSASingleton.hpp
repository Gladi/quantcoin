#pragma once

#include <string>
#include <mutex>

struct RSASingleton {
    static std::string GetPublicKey() noexcept;
    static void SetPublicKey(std::string public_key) noexcept;

    static std::string GetPrivateKey() noexcept;
    static void SetPrivateKey(std::string private_key) noexcept;

private:
    static std::string _public_key;
    static std::string _private_key;
    static std::mutex mutex;
};