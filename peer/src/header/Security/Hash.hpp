#pragma once

#include <random>
#include <string>
#include "openssl/sha.h"

#include "header/Blockchain/Account/AccountError.hpp"

struct Hash {
  using sha1_t = std::string;

  static void simpleSHA1(unsigned char* input,
                         unsigned long length,
                         unsigned char* md);

  [[nodiscard]] static std::string GenerateRandomSHA1(
      const size_t& length_for_random = 2000);

  [[nodiscard]] static std::string GenerateSHA256(std::string text);

  static std::string GenerateRandomString(const size_t& len);

 private:
  template <size_t SIZE_FOR_HASH>
  inline static std::string ConvertHashToString(unsigned char* md) {
    char* result =
        static_cast<char*>(malloc(sizeof(char) * ((SIZE_FOR_HASH * 2) + 1)));
    result[SIZE_FOR_HASH * 2] = '\0';

    for (size_t i = 0; i < SIZE_FOR_HASH; ++i)
      sprintf(&result[i * 2], "%02x", md[i]);

    return result;
  };
};
