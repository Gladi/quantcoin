#pragma once

#include <filesystem>
#include <string>
#include "fstream"
#include "openssl/pem.h"
#include "openssl/rsa.h"

#include "config.hpp"
#include "header/Blockchain/Account/AccountError.hpp"
#include "RSASingleton.hpp"

using public_key_t = std::string;
using private_key_t = std::string;
namespace fs = std::filesystem;

namespace Crypto {

namespace RSA_API {
enum class MODE_FOR_FILE { public_key, private_key };

template <MODE_FOR_FILE ModeForFile>
[[nodiscard]] std::string one_file_write_and_delete(RSA* rsa) {
  constexpr auto path_temp = "temp_rsa.key";

  FILE* fp = fopen(path_temp, "wr");

  if (fp == NULL)
    throw AccountError(AccountError::Type::GetPublicAndPrivateKeyFile);

  int result_write;
  if constexpr (ModeForFile == MODE_FOR_FILE::public_key)
    result_write = PEM_write_RSAPublicKey(fp, rsa);
  else if constexpr (ModeForFile == MODE_FOR_FILE::private_key)
    result_write = PEM_write_RSAPrivateKey(fp, rsa, nullptr, nullptr, NULL,
                                           nullptr, nullptr);

  if (result_write == 0)
    throw AccountError(AccountError::Type::GetPublicAndPrivateKeyFile);

  fclose(fp);
  std::fstream file;
  file.open(path_temp, std::ios::in | std::ios::out);
  auto result = std::string((std::istreambuf_iterator<char>(file)),
                            std::istreambuf_iterator<char>());

  file.close();
  fs::remove(path_temp);
  return result;
};

[[nodiscard]] static std::tuple<public_key_t, private_key_t>
GetPublicAndPrivateKey(RSA* rsa) {
  return std::make_tuple(
      one_file_write_and_delete<MODE_FOR_FILE::public_key>(rsa),
      one_file_write_and_delete<MODE_FOR_FILE::private_key>(rsa));
};

[[nodiscard]] static std::tuple<public_key_t, private_key_t> GenerateRSA(
    const unsigned int& size_key = compiler_config::SIZE_RSA_KEY) {
  // Generate
  RSA* rsa = RSA_generate_key(size_key, RSA_F4, nullptr, nullptr);

  return GetPublicAndPrivateKey(rsa);
};

template <size_t SIZE_RSA_KEY = compiler_config::SIZE_RSA_KEY>
[[nodiscard]] static std::tuple<std::string, bool> Encrypt(
    const std::string& message,
    const private_key_t& private_key = RSASingleton::GetPrivateKey()) {
  RSA* rsa;
  constexpr auto path_temp = "temp_rsa.key";

  if (fs::exists(path_temp))
      fs::remove(path_temp);

  std::fstream file_for_write;
  file_for_write.open(path_temp, std::ios::in | std::ios::out | std::ios::app);
  file_for_write << private_key;
  file_for_write.flush();
  file_for_write.close();

  FILE* fp = fopen(path_temp, "r");
  rsa = PEM_read_RSAPrivateKey(fp, nullptr, nullptr, nullptr);

  int len = RSA_size(rsa);
  auto *buf = new unsigned char[len];
  memset(buf, 0, len);
  bool is_ok;
  if (RSA_private_encrypt(
          message.size(), (const unsigned char*)message.c_str(),
          buf, rsa, RSA_PKCS1_PADDING) != -1)
    is_ok = true;
  else
    is_ok = false;

  fclose(fp);
  fs::remove(path_temp);
  RSA_free(rsa);

  return std::make_tuple(std::string(reinterpret_cast<char*>(buf)), is_ok);
}

template <size_t SIZE_RSA_KEY = compiler_config::SIZE_RSA_KEY>
[[nodiscard]] static std::tuple<std::string, bool> Decrypt(
    const unsigned char* message,
    const public_key_t& public_key) {
  unsigned char* out;
  bool is_ok;
  RSA* rsa = RSA_new();
  constexpr auto path_temp = "temp_rsa.key";
  FILE* fp = fopen(path_temp, "wr");
  fwrite(public_key.c_str(), public_key.size(), public_key.size(), fp);
  PEM_read_RSAPrivateKey(fp, &rsa, nullptr, nullptr);

  if (RSA_public_decrypt(SIZE_RSA_KEY, message, out, rsa, RSA_NO_PADDING) != -1)
    is_ok = true;
  else
    is_ok = false;

  fclose(fp);
  fs::remove(path_temp);
  RSA_free(rsa);

  return std::make_tuple(std::string(reinterpret_cast<char*>(out)), is_ok);
}

};  // namespace RSA_API

};  // namespace Crypto
