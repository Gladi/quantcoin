//
// Created by gladi on 07.12.2021.
//

#pragma once

#include <mutex>

namespace qstd {
    template <typename T>
    class mutex_object final {
    public:
     mutex_object(T&& object) noexcept {
       this->object = new T(object);
     };

     mutex_object(T& object) noexcept {
       this->object = new T(object);
     };

     mutex_object() noexcept = default;

     ~mutex_object() noexcept {
       //if (object != nullptr)
       //  delete object; IN STACK
     }

        T* operator->() noexcept {
          std::lock_guard<std::mutex> lock(mutex);
          return object;
        };

        T operator*() noexcept {
          std::lock_guard<std::mutex> lock(mutex);
          return *object;
        }

        void swap(T&& new_object) {
          std::lock_guard<std::mutex> lock(mutex);
          object = new T(new_object);
        }

        [[nodiscard]] T copy() noexcept {
          std::lock_guard<std::mutex> lock(mutex);
          return *object;
        };

        [[nodiscard]] T* get() noexcept {
          std::lock_guard<std::mutex> lock(mutex);
          return object;
        };

        [[nodiscard]] bool is_exists() noexcept {
          return object;
        };

    private:
        std::mutex mutex;
        T* object = nullptr;
    };
};