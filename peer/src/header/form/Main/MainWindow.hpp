#pragma once

#include <QClipboard>
#include <QDialog>

#include "header/Blockchain/Blockchain.hpp"
#include "header/Settings/Settings.hpp"
#include "header/form/Closing/Closing.hpp"
#include "header/form/FormService.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QDialog {
  Q_OBJECT

 public:
  explicit MainWindow(QWidget* parent = nullptr);
  ~MainWindow();

 private slots:
  void on_MainWindow_destroyed();

  void on_MainWindow_finished();

  void on_create_account_bt_clicked();

  void on_copy_your_wallet_address_bt_clicked();

 private:
  Ui::MainWindow* ui;  //-V122

  inline void SetDesing() noexcept {
    FormService::SetWindowTitle(this, "Main");
    FormService::SetNoChanged(this);
  }

  [[nodiscard]] inline bool HasAccount() {
    auto account = SettingsManager::Load().account_info;
    return !(account.wallet_address.empty());
  }

  [[nodiscard]] void CheckHasAccount();

  [[nodiscard]] void ShowInfoAboutInfoBlockchain();
};
