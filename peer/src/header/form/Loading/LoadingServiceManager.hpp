//
// Created by gladi on 02.01.2022.
//

#pragma once

#include "header/Service/ServiceStartManager.hpp"
#include "header/Service/Services/AllService.hpp"

class LoadingServiceManager {
 public:
  static service_manager::vector_iservices_t GetServices() noexcept;

 private:
  static void AddCoreServices(
      service_manager::vector_iservices_t& vector_iservices) noexcept;
};
