#pragma once

#include <QMainWindow>
#include <QMessageBox>
#include <QProgressBar>
#include <mutex>

#include "LoadingServiceManager.hpp"
#include "config.hpp"
#include "header/QSTL/tools/is_in.hpp"
#include "header/form/FormService.hpp"
#include "header/form/Main/MainWindow.hpp"

QT_BEGIN_NAMESPACE
namespace Ui {
class Loading;
}
QT_END_NAMESPACE

//! Loading form
class Loading final : public QMainWindow {
  Q_OBJECT

 public:
  Loading(QWidget* parent = nullptr);
  ~Loading();

 private:
  Ui::Loading* ui;  //-V122

  void AllLoadingWork() noexcept;

  static void done_work(
      service_manager::work_services_output output,
      const service_manager::message_for_error_t& message_for_error,
      QWidget* widget_loading) noexcept;

  static void done_one_service(QProgressBar* progress) noexcept;

  //! For sync signals and QProgressBar
  //! \warning I know that boost::signal2 is thread-safe, but I'm worried about
  //! safety anyway. It use in connect_all_signal because mutex is no static
  std::mutex mutex;

  __attribute__((always_inline)) void connect_all_signal(
      sboost::signal<void()>& done_one_service,
      service_manager::signal_for_all_work_done_t& done_work,
      QProgressBar* progress_loading_service,
      QWidget* widget_loading) {
    done_one_service.connect([&]() {
      std::lock_guard<std::mutex> lock(mutex);
      Loading::done_one_service(progress_loading_service);
    });

    done_work.connect(
        [&](service_manager::work_services_output output,
            const service_manager::message_for_error_t& message_for_error) {
          std::lock_guard<std::mutex> lock(mutex);
          Loading::done_work(output, message_for_error, widget_loading);
        });
  };
};
