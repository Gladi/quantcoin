//
// Created by gladi on 04.01.2022.
//

#pragma once

#include <QApplication>
#include <QDesktopWidget>
#include <QDialog>
#include <QStyle>

#include "config.hpp"

class FormService {
 public:
  static void WindowSwitch(QWidget* your_window,
                           QDialog& new_window_for_open) noexcept;

  static void GetCenter(QWidget& window) noexcept;

  static void HideClose(QWidget& window) noexcept;

  static void HideQuestion(QWidget& window) noexcept;

  //! Set window title
  //! \code
  //! FormService::SetWindowTitle(this, "Test form");
  //! \endcode
  static void SetWindowTitle(QWidget* window,
                             const QString& text,
                             const QString& name_program =
                                 compiler_config::NAME_CRYPTOCURRENCY) noexcept;

  static void SetNoChanged(QWidget* window) noexcept;

 private:
  FormService() = delete;
  ~FormService() = delete;
};
