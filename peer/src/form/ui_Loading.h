/********************************************************************************
** Form generated from reading UI file 'Loading.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOADING_H
#define UI_LOADING_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Loading
{
public:
    QWidget *centralwidget;
    QProgressBar *progress_loading_service;
    QLabel *label_loading;

    void setupUi(QMainWindow *Loading)
    {
        if (Loading->objectName().isEmpty())
            Loading->setObjectName(QString::fromUtf8("Loading"));
        Loading->setWindowModality(Qt::NonModal);
        Loading->resize(662, 142);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/resource/icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        Loading->setWindowIcon(icon);
        Loading->setLayoutDirection(Qt::LeftToRight);
        centralwidget = new QWidget(Loading);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        progress_loading_service = new QProgressBar(centralwidget);
        progress_loading_service->setObjectName(QString::fromUtf8("progress_loading_service"));
        progress_loading_service->setGeometry(QRect(10, 90, 631, 41));
        progress_loading_service->setValue(0);
        label_loading = new QLabel(centralwidget);
        label_loading->setObjectName(QString::fromUtf8("label_loading"));
        label_loading->setGeometry(QRect(11, 0, 621, 81));
        QFont font;
        font.setPointSize(36);
        label_loading->setFont(font);
        Loading->setCentralWidget(centralwidget);

        retranslateUi(Loading);

        QMetaObject::connectSlotsByName(Loading);
    } // setupUi

    void retranslateUi(QMainWindow *Loading)
    {
        Loading->setWindowTitle(QCoreApplication::translate("Loading", "Loading", nullptr));
        label_loading->setText(QCoreApplication::translate("Loading", "Loading...", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Loading: public Ui_Loading {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOADING_H
