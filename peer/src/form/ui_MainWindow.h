/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QStackedWidget *stackedWidget;
    QWidget *page_sync_blockchain;
    QLabel *blockchain_is_old_label;
    QLabel *sync_label;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *status_label;
    QLabel *connection_peers_label;
    QLabel *total_blocks_label;
    QLabel *have_blocks_label;
    QLabel *you_root_hash_label;
    QLabel *peers_root_hash_label;
    QWidget *page_account_is_found;
    QPushButton *send_bt;
    QLabel *balance_lb;
    QLabel *recent_transactions_lb;
    QPushButton *copy_your_wallet_address_bt;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout;
    QLabel *send_to_lb;
    QLineEdit *send_to_label_edit;
    QWidget *page_not_found_account;
    QLabel *account_not_found_label;
    QPushButton *create_account_bt;

    void setupUi(QDialog *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(921, 535);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/resource/icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        stackedWidget = new QStackedWidget(MainWindow);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 921, 531));
        page_sync_blockchain = new QWidget();
        page_sync_blockchain->setObjectName(QString::fromUtf8("page_sync_blockchain"));
        blockchain_is_old_label = new QLabel(page_sync_blockchain);
        blockchain_is_old_label->setObjectName(QString::fromUtf8("blockchain_is_old_label"));
        blockchain_is_old_label->setGeometry(QRect(210, 0, 471, 111));
        QFont font;
        font.setPointSize(36);
        blockchain_is_old_label->setFont(font);
        blockchain_is_old_label->setStyleSheet(QString::fromUtf8("color: rgb(170, 0, 0)"));
        blockchain_is_old_label->setAlignment(Qt::AlignCenter);
        sync_label = new QLabel(page_sync_blockchain);
        sync_label->setObjectName(QString::fromUtf8("sync_label"));
        sync_label->setGeometry(QRect(340, 390, 241, 81));
        sync_label->setFont(font);
        sync_label->setStyleSheet(QString::fromUtf8("color: rgb(170, 0, 0)"));
        sync_label->setAlignment(Qt::AlignCenter);
        layoutWidget = new QWidget(page_sync_blockchain);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 100, 921, 301));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        status_label = new QLabel(layoutWidget);
        status_label->setObjectName(QString::fromUtf8("status_label"));
        QFont font1;
        font1.setPointSize(20);
        status_label->setFont(font1);
        status_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout->addWidget(status_label);

        connection_peers_label = new QLabel(layoutWidget);
        connection_peers_label->setObjectName(QString::fromUtf8("connection_peers_label"));
        connection_peers_label->setFont(font1);
        connection_peers_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout->addWidget(connection_peers_label);

        total_blocks_label = new QLabel(layoutWidget);
        total_blocks_label->setObjectName(QString::fromUtf8("total_blocks_label"));
        total_blocks_label->setFont(font1);
        total_blocks_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout->addWidget(total_blocks_label);

        have_blocks_label = new QLabel(layoutWidget);
        have_blocks_label->setObjectName(QString::fromUtf8("have_blocks_label"));
        have_blocks_label->setFont(font1);
        have_blocks_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout->addWidget(have_blocks_label);

        you_root_hash_label = new QLabel(layoutWidget);
        you_root_hash_label->setObjectName(QString::fromUtf8("you_root_hash_label"));
        you_root_hash_label->setFont(font1);
        you_root_hash_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout->addWidget(you_root_hash_label);

        peers_root_hash_label = new QLabel(layoutWidget);
        peers_root_hash_label->setObjectName(QString::fromUtf8("peers_root_hash_label"));
        peers_root_hash_label->setFont(font1);
        peers_root_hash_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout->addWidget(peers_root_hash_label);

        stackedWidget->addWidget(page_sync_blockchain);
        page_account_is_found = new QWidget();
        page_account_is_found->setObjectName(QString::fromUtf8("page_account_is_found"));
        send_bt = new QPushButton(page_account_is_found);
        send_bt->setObjectName(QString::fromUtf8("send_bt"));
        send_bt->setGeometry(QRect(150, 480, 80, 26));
        balance_lb = new QLabel(page_account_is_found);
        balance_lb->setObjectName(QString::fromUtf8("balance_lb"));
        balance_lb->setGeometry(QRect(0, 10, 921, 41));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Arial"));
        font2.setPointSize(22);
        font2.setItalic(false);
        balance_lb->setFont(font2);
        balance_lb->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        recent_transactions_lb = new QLabel(page_account_is_found);
        recent_transactions_lb->setObjectName(QString::fromUtf8("recent_transactions_lb"));
        recent_transactions_lb->setGeometry(QRect(630, 30, 241, 51));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Arial"));
        font3.setPointSize(20);
        recent_transactions_lb->setFont(font3);
        recent_transactions_lb->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        copy_your_wallet_address_bt = new QPushButton(page_account_is_found);
        copy_your_wallet_address_bt->setObjectName(QString::fromUtf8("copy_your_wallet_address_bt"));
        copy_your_wallet_address_bt->setGeometry(QRect(740, 500, 171, 26));
        layoutWidget1 = new QWidget(page_account_is_found);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 440, 361, 28));
        horizontalLayout = new QHBoxLayout(layoutWidget1);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        send_to_lb = new QLabel(layoutWidget1);
        send_to_lb->setObjectName(QString::fromUtf8("send_to_lb"));

        horizontalLayout->addWidget(send_to_lb);

        send_to_label_edit = new QLineEdit(layoutWidget1);
        send_to_label_edit->setObjectName(QString::fromUtf8("send_to_label_edit"));

        horizontalLayout->addWidget(send_to_label_edit);

        stackedWidget->addWidget(page_account_is_found);
        page_not_found_account = new QWidget();
        page_not_found_account->setObjectName(QString::fromUtf8("page_not_found_account"));
        account_not_found_label = new QLabel(page_not_found_account);
        account_not_found_label->setObjectName(QString::fromUtf8("account_not_found_label"));
        account_not_found_label->setGeometry(QRect(0, 10, 911, 171));
        QFont font4;
        font4.setPointSize(48);
        account_not_found_label->setFont(font4);
        account_not_found_label->setStyleSheet(QString::fromUtf8("color:  rgb(170, 0, 0)"));
        account_not_found_label->setAlignment(Qt::AlignCenter);
        create_account_bt = new QPushButton(page_not_found_account);
        create_account_bt->setObjectName(QString::fromUtf8("create_account_bt"));
        create_account_bt->setGeometry(QRect(200, 300, 521, 121));
        create_account_bt->setFont(font4);
        stackedWidget->addWidget(page_not_found_account);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QDialog *MainWindow)
    {
        MainWindow->setWindowTitle(QString());
        blockchain_is_old_label->setText(QCoreApplication::translate("MainWindow", "Blockchain is old!", nullptr));
        sync_label->setText(QCoreApplication::translate("MainWindow", "Sync...", nullptr));
        status_label->setText(QCoreApplication::translate("MainWindow", "Status:", nullptr));
        connection_peers_label->setText(QCoreApplication::translate("MainWindow", "Connection peers: 0", nullptr));
        total_blocks_label->setText(QCoreApplication::translate("MainWindow", "Total blocks: 0", nullptr));
        have_blocks_label->setText(QCoreApplication::translate("MainWindow", "Have blocks: 0", nullptr));
#if QT_CONFIG(whatsthis)
        you_root_hash_label->setWhatsThis(QString());
#endif // QT_CONFIG(whatsthis)
        you_root_hash_label->setText(QCoreApplication::translate("MainWindow", "You root hash: NOT", nullptr));
        peers_root_hash_label->setText(QCoreApplication::translate("MainWindow", "Peers root hash: NOT", nullptr));
        send_bt->setText(QCoreApplication::translate("MainWindow", "Send", nullptr));
        balance_lb->setText(QCoreApplication::translate("MainWindow", "Balance: 0", nullptr));
        recent_transactions_lb->setText(QCoreApplication::translate("MainWindow", "Recent transactions", nullptr));
        copy_your_wallet_address_bt->setText(QCoreApplication::translate("MainWindow", "Copy your wallet address", nullptr));
        send_to_lb->setText(QCoreApplication::translate("MainWindow", "To:", nullptr));
        account_not_found_label->setText(QCoreApplication::translate("MainWindow", "ACCOUNT NOT FOUND!", nullptr));
        create_account_bt->setText(QCoreApplication::translate("MainWindow", "Create account", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
